#!/bin/sh

server=mybackup@151.80.135.63
sourceDir=$HOME/Dokumenty
serverPath=backup

date=`date "+%Y-%m-%dT%H:%M:%S"`

# Rsync będzie wykonywał backup to podanegeo katalogu na serwerze. Za każdym razem będzie tworzył backup 
# 	do nowego katalogu zawierającego w nazwie datę backupu
# 
# --link-dest - dzięki tej opcji nasz backup będzie "inkrementalny" - nie będziemy wysyłać niezmienionych
# 	plików, tylko będą do tych plików tworzone hardlinki w każdej kolejnej wersji backupu
# -az - włączamy tryb "archive" i kompresujemy przesyłane dane
rsync -azv --progress --link-dest=\$HOME/${serverPath}/current ${sourceDir} ${server}:${serverPath}/old-${date}


# linkujemy katalog ${serverPath}/current) do aktualnej wersji backupu
ssh ${server} "(test ! -d ${serverPath}/current || rm ${serverPath}/current) && ln -s old-${date} ${serverPath}/current"