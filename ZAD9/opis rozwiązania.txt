
Środowisko:
	Klient: Ubuntu 14.04
	Serwer: Ubuntu 15.04

Poniższe kroki można powtórzyć korzystając w postawionego serwera: mybackup@151.80.135.63
Wystarczy wtedy powtórzyć kroki z sekcji 'klient' poniższej instrukcji. 
Hasło do hosta to: iy4o5lsh


KROKI DO WYKONANIA:

serwer:
	sudo adduser mybackup

	su - mybackup
	mkdir backup
	sudo apt-get install rsync

klient:
	ssh-keygen -t rsa
	# zostawiamy domyślne wartości

	# kopiujemy klucz publiczny na serwer, żeby można było się łączyć bez hasła
	ssh-copy-id mybackup@151.80.135.63 

	# w skrypcie backup.sh podmieniamy zmienne na własne dane:
	# 	server     - dane połączenia do serwera po ssh w formacie <user>@<host>, 
	#				 jeśli korzystamy w przykładowego serwera zostawiamy
	#				 mybackup@151.80.135.63 
	#   sourceDir  - katalogu lokalny, który chcemy backupować
	#   serverPath - katalog zdalny, do którego będzie tworzony backup

	chmod +x backup.sh
	sudo cp backup.sh /opt
	sudo chown jozef:jozef /opt/backup.sh

	# tworzymy plik logu z odpowiednimi uprawnieniami
	sudo touch /var/log/mybackup.log
	sudo chown jozef:jozef /var/log/mybackup.log

	cronteab -e
	# dopisujemy linię:
	*/1 * * * * /opt/backup.sh >> /var/log/mybackup.log 2>&1

	# gotowe, backup będzie tworzony co minutę. Tylko zmienione pliki będą przesyłane. 
	# Będziemy mieć dostęp do historii plików (backup inkrementalny)


Sprawdzania działania, debugowanie
	klient:
		# podglądamy log programu
		tail -f /var/log/mybackup.log
